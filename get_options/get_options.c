/*
	make: 	g++     -Wno-write-strings -o test *.c
		clang++ -Wno-write-strings -o test *.c

		or

		gcc   -o test *.c
		clang -o test *.c


	упрощенная версия разбора опций коммандной строки
	=================================================
	ОПЦИИ ИДУТ ВНАЧАЛЕ!!!
	use: cmd [options] прочие параметры
	иначе неверно расчитает последнее значение и не понятно, что делать в этом случае, пока просто игнорируем.

	формат опций:
	optname value
	optname=value
	optname

	пример:
	cmd -opt1=val1 -opt2 val2 --opt3=val3 --opt4 val4 ...

	предварительное тире можно заменть на дргуой символ, главное что бы было отличие опции от свободного параметра


	Каждая опция описывается 4-мя параметрами:
	==========================================

	typedef struct {
	        char *name;	//имя опции: --mylen -p, включая тире, т.е. описывает как вводим в сом.строке
	        int   flag;     //признак наличия данных =1, 0 - нет данных (как строки рассматриваем)
	        char *value;    //ссылка на последнее вычисленое значение опции или опции по умолчанию,
				//всегда текст, в число преобразуем уже в основном теле программы или когда необходимо.
	        int  count;     //счетчик повторов опции, >0 значит опция была введена, через cом.строку
				//некоторые опции можно повторять, для логических операций, например: test -d -d -d
				//счетчик будет ==3, возможно, что кому то требуется такой финт.
	} option_t;
*/

#ifdef __cplusplus
extern "C" {
#endif

#include "get_options.h"

static char *__ID ="ver: 1.0, ID: get_options.c, mail: onix@orel.ru, datime: "__DATE__ " " __TIME__;

//дублируем строку, что бы отвязаться от args
#define get_options_str_dump(str) strdup(str)


// внутренняя операция, ищем опцию и вертаем описание, что нашли или NULL
static option_t *get_options_find(char *name, option_t *options)
{
	int i;

	for (i=0; options[i].name; i++) {
		//сравнение всегда с регистром
		if (strcmp(name, options[i].name) ==0)
			return (option_t *) &options[i];
	}

	return NULL;
}

// прочитает значение по умолчанию или после ввода
// можно на прямую, но так проще
char *get_options_value(char *name, option_t *options)
{
	option_t *o =get_options_find(name, options);
	return (o)? o->value: NULL;
}

// вернет счетчик повторений
int get_options_count(char *name, option_t *options)
{
	option_t *o =get_options_find(name, options);
	return (o)? o->count: 0;
}

//вернет версию данного продукиа
char *get_options_version(void)
{
	return __ID;
}


//
// основной разбор параметров, заполнит структуру значениями, если найдет
// <0  - ошибка разбора опций, укажет позицию где ошибка
// >=0 - с этой позиции начинаются параметры без опций
// надо бы так назвать:
//
int get_options(int argc, char **argv, option_t *option)
{
	int ret_argc =0;
	int i =0;
	option_t *o;
	//аргументы переданные в формате -arg=par
	char *p =NULL;		//позиция, где найден разделитель

	do {
		//проверяем опцию на формат option=value или option value
		p =strchr(argv[i],'=');
		//нашли, делим
		if (p) {
			int len =p -argv[i];
			argv[i][len] ='\0';	//обрежем опцию, будет нормально искаться в структуре
			p++;			//мы стоим на символе '=', надо сметиться
		}

		//поиск опции
		o =get_options_find(argv[i], option);

		//если нашли, обработаем
		if (o) {
			//нашли опцию с параметром?
			if (o->flag) {
				//какой был формат?
				if (p) {
					o->value =p;	//с '='
				} else {
					i++;		//с пробелом
					//проверим допустимые границы кол-ва аргументов
					if (i <argc)
						o->value =argv[i];	//с '='
					else
						//при ошибке вернем, где остановились с минусом
						return -i;
				}
			}

			o->count++;	//скажем, что опцию вводили хотя бы 1 раз.
			ret_argc =i;	//запомним последний указатель, для определения границы опций
		}	//end: if (o)

		i++;
	} while (i <argc);

	//надо вернуть следующую позицию, поэтому +1
	//может превысить реальное кол-во аргументов, учтите это.
	return (ret_argc <i)? (ret_argc +1): -i;
}


//распечатать содержимое структуры, в качестве help
int get_options_print(option_t *options)
{
	int i;

	printf("[options, format: name [new_value |default] count]\n");
	for (i =0; options[i].name; i++) {
		if (options[i].flag) {
			printf("\t%s '%s' %d",options[i].name, options[i].value, options[i].count);
		} else {
			printf("\t%s %d",options[i].name, options[i].count);
		}
		printf("\n");
	}
	printf("[options end]\n");

	return 0;
}



#ifdef __cplusplus
}
#endif

#ifndef _GET_OPTIONS_H
#define _GET_OPTIONS_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
        char *name;             //полное имя опции: "--mylen", "-p" ...
        int   flag;             //признак наличия данных: =1 - есть, =0 - нет данных.
        char *value;            //ссылка на последнее вычисленое значение опции или опции по умолчанию
        int  count;             //счетчик повторов опции, >0 значит опция была введена, через ком-строку
} option_t;

//основной разбор параметров
#define get_options_parse get_options

int   get_options(int argc, char **argv, option_t *option);
//распечатать структуру
int   get_options_print(option_t *options);
//по имени опции получить значение
char *get_options_value(char *name, option_t *options);
//по имени опции получить счетчик
int   get_options_count(char *name, option_t *options);
//вернет версию библиотеки
char *get_options_version(void);


#endif

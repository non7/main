#ifdef __cplusplus
extern "C" {
#endif


#include "get_options.h"


int main(int argc, char **argv)
{
	char *procname;
	option_t oo[] ={
		{ "+w",   		1, 	NULL, 		0 },	//для накопления первое значчение ==NULL
		{ "-a",   		0, 	NULL, 		0 },
		{ "-ab",  		1, 	  "", 		0 },
		{ "long-long-long",	0, 	NULL, 		0 },
		{ "--config",   	1, 	"init.xml", 	0 },
		{ "--time"  ,   	1, 	"10",   	0 },
		{ "--intv"  ,   	1,  	 "1",  		0 },
		{ "--probes",   	1,  	 "2",   	0 },
		{ "--dbg"   ,   	1,  	 "0",   	0 },
		{ "--log"   ,   	1,  	 "1",   	0 },
		{ "--logdir",   	1, 	"./",   	0 },
		{ "--stop"  ,   	1, 	"20",   	0 },
		{ "--interval", 	1,  	 "5",   	0 },
		{ NULL    , 		0, 	NULL, 		0 }
	};

	//0-м параметром передается имя программы, нам она не нужна
	procname =argv[0];

	argc--;
	argv++;

	int i;

	if (argc >0) {
		printf("proc='%s'\nver='%s'\n", procname, get_options_version());

		i =get_options_parse(argc, argv, oo);
		if (i <0) {
			printf("*ER: stop parse options: argv[%d]='%s'\n", -i, argv[(-i-1)]);
			return -1;
		}
		get_options_print(oo);

		printf("next params data position=%d\n", i);

		for (i =0; i <argc; i++) {

			printf("argv[%2d] -> %s", i, argv[i]);

			char *val =get_options_value(argv[i], oo);
			int count =get_options_count(argv[i], oo);

			if (val)
				printf(" ='%s'", val);

			printf(" :%d\n",count);
		}
	} else {
		printf ("not options\n");
	}

	return 0;
}


#ifdef __cplusplus
}
#endif

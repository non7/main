//
// (L) onix@orel.ru 
// 


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


/*
 * The GOST 28147-89 cipher + fishka!!!!
 * The "fishka" is a way to change the key.
 * #фишка - меняет ключ одновременно с данными
 * This code is placed in the public domain.
 */

static uint8_t k8[16] ={ 14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7 }; 
static uint8_t k7[16] ={ 15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10 };
static uint8_t k6[16] ={ 10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8 };
static uint8_t k5[16] ={  7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15 };
static uint8_t k4[16] ={  2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9 };
static uint8_t k3[16] ={ 12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11 };
static uint8_t k2[16] ={  4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1 };
static uint8_t k1[16] ={ 13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7 };

/* uint8_t-at-a-time substitution boxes */
static uint8_t k87[256];
static uint8_t k65[256];
static uint8_t k43[256];
static uint8_t k21[256];

/*
 * Build uint8_t-at-a-time subtitution tables.
 * This must be called once for global setup.
 */
void kboxinit(void)
{
	int i;

	for (i =0; i <256; i++) {
		k87[i] =k8[i >>4] <<4 | k7[i &15];
		k65[i] =k6[i >>4] <<4 | k5[i &15];
		k43[i] =k4[i >>4] <<4 | k3[i &15];
		k21[i] =k2[i >>4] <<4 | k1[i &15];
	}
}

static inline uint32_t _f(uint32_t x)
{
	x =	k87[x >>24 &255] <<24 |
		k65[x >>16 &255] <<16 |
	    	k43[x >> 8 &255] << 8 |
		k21[x      &255];
	/* Rotate left 11 bits */
	return (x <<11) | (x >>21);
}

//базовое преобразлвание
void gost_code(uint32_t *inp, uint32_t *out, uint32_t *key, int rf, int rr)
{
	uint32_t n[2];
	int i;

	n[0] = inp[0], n[1] = inp[1];

	/* Instead of swapping halves, swap names each round */
	for(i =0; i <rf; i++) {
		key[0] +=n[1] ^=_f(n[0] +key[0]); key[1] +=n[0] ^=_f(n[1] +key[1]);
		key[2] +=n[1] ^=_f(n[0] +key[2]); key[3] +=n[0] ^=_f(n[1] +key[3]);
		key[4] +=n[1] ^=_f(n[0] +key[4]); key[5] +=n[0] ^=_f(n[1] +key[5]);
		key[6] +=n[1] ^=_f(n[0] +key[6]); key[7] +=n[0] ^=_f(n[1] +key[7]);
		//     ^---fishka                        ^---fishka
	}

	for(i =0; i <rr; i++) {
		key[7] +=n[1] ^=_f(n[0] +key[7]); key[6] +=n[0] ^=_f(n[1] +key[6]);
		key[5] +=n[1] ^=_f(n[0] +key[5]); key[4] +=n[0] ^=_f(n[1] +key[4]);
		key[3] +=n[1] ^=_f(n[0] +key[3]); key[2] +=n[0] ^=_f(n[1] +key[2]);
		key[1] +=n[1] ^=_f(n[0] +key[1]); key[0] +=n[0] ^=_f(n[1] +key[0]);
		//     ^---fishka                        ^---fishka
	}

	/* There is no swap after the last round */
	out[0] = n[1], out[1] = n[0];
}


#define C0 0x01010101
#define C1 0x01010104

void gost_code_ofb(uint32_t *inp, uint32_t *out, long len, uint32_t *iv, uint32_t *key_inp)
{
	uint32_t key[8];	//work key
	uint32_t temp[2];       //Counter
	uint32_t gamma[2];      //Output XOR value
	long i;

	memcpy(key, key_inp, sizeof(key));

	//Compute starting value for counter
	gost_code(iv, temp, key, 3, 1);

	for(i =0; i <len; i+=2) {
		//Wrap modulo 2^32? , Make it modulo 2^32-1
		temp[0] +=C0, temp[1] +=C1;

		if (temp[1] <C1)
			temp[1]++;

		gost_code(temp, gamma, key, 3, 1);

		out[i +0] =inp[i +0] ^gamma[0];
		out[i +1] =inp[i +1] ^gamma[1];
	}

	//save new iv
	gost_code(gamma, iv, key, 2, 2);
}

/*
 * The message suthetication code uses only 16 of the 32 rounds.
 * There *is* a swap after the 16th round.
 * The last block should be padded to 64 bits with zeros.
 * len is the number of *blocks* in the input.
 */
void gost_code_mac(uint32_t *buf, long len, uint32_t *mac_inp, uint32_t *mac_out, uint32_t *key)
{
	long i;
	uint32_t n[2];

	n[0] =mac_inp[0], n[1] =mac_inp[1];

	for(i =0; i <len; i+=2) {
		n[0] ^=buf[i+0], n[1] ^=buf[i+1];

		gost_code(n, n, key, 1, 1);
	}

	mac_out[0] =n[0], mac_out[1] =n[1];
}





//// T E S T ////////////////////////////////////////////////////////////////

static uint32_t key[8];
static uint32_t plain[8];
static uint32_t cipher[8];
static uint32_t out2[8];
static uint32_t iv[2];
static uint32_t mac[2];

int main(void)
{
	int i;

	kboxinit();

	plain[0] =0xA5A5A5A5;
	plain[1] =0x5A5A5A5A;

	gost_code_ofb(plain, cipher,2, iv, key);
	printf("gost_encode: %08X %08X -> %08X %08X\n",plain[0],plain[1], cipher[0],cipher[1]);
	printf("iv next:     %08X %08X\n", iv[0], iv[1]);

	iv[0] =iv[1] =0;

	gost_code_ofb(cipher, out2, 2, iv, key);
	printf("gost_decode: %08X %08X -> %08X %08X\n",cipher[0],cipher[1], out2[0],out2[1]);
	printf("iv next:     %08X %08X\n", iv[0], iv[1]);

	gost_code_mac(plain, 8, mac, mac, key);

	printf("gost_code_mac: %08X %08X\n",mac[0],mac[1]);

	return 0;
}



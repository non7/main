@echo off

del *.exe

for %%I in (2 4 6 8) do (

	call c:\bin\cc19x64 /Ox /DMB_X_MAX=%%I test-time-next.c     /Fetest-time-next%%Ix64.exe
	call c:\bin\cc19x64 /Ox /DMB_X_MAX=%%I test-next-le.c       /Fetest-next-le%%Ix64.exe
	call c:\bin\cc19x64 /Ox /DMB_X_MAX=%%I test-generate-next.c /Fetest-generate-next%%Ix64.exe
	call c:\bin\cc19x64 /Ox /DMB_X_MAX=%%I test-hash.c          /Fetest-hash%%Ix64.exe
	call c:\bin\cc19x64 /Ox /DMB_X_MAX=%%I test-code.c          /Fetest-code%%Ix64.exe
rem	call c:\bin\cc19x64 /Ox /DMB_X_MAX=%%I test-rotN-next.c     /Fetest-rotN-next%%Ix64.exe
)

del *.obj


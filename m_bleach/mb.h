#ifndef __mb_h
#define __mb_h 1

/*

УНИВЕРСАЛЬНЫЙ ГЕНЕРАТОР ПОТОКА КЛЮЧЕЙ ИЛИ ХЕШ ФУНКЦИЯ

название метода: множественное отбеливание (multiple bleaching -> mb)

данного метода, нет в криптографии, но схемы подобного типа, используют постоянно
здесь просто произведено обобщение всех похожих методов в один способ.


ОСНОВНАЯ ИДЕЯ:

next(x) {
	n = x;
	for r (1..round) {
		s = rot(sum(n[1]+...+n[N]))
		for i (1..N) {
			n[i] = n[i] xor s, s = n[i];
		}
	}
	x = x xor n;
}

в терминах С:

static inline void __mb_next_4(uintxx_t *x)
{
	int i;
	uintxx_t n[4];

	//сначала раскидаем входные значения в свои переменные
	n[0] =x[0], n[1] =x[1], n[2] =x[2], n[3] =x[3];

	//генератор для arx схемы, используем схему от ГОСТ89, но с другими константами,
	//они веселей биты шевелят. можно упростить и сделать как в chacha/salsa n++,
	//потратит больше циклов на "разгон"
	//

	MB_NEXT_GEN();

	//базовый микс, можно отказаться от генератора, но в этому случае,
	//наличие '1' в битах обязательно (хотя бы 1 бит), иначе реально 0 на выходе получится.
	//это единственное условие. ну и подольше разгоняется,
	//(придется увеличивать в 2 раза кол-во циклов).

	for(i =0; i <MB_R_MAX; i++) {

		//не смотря на кажущуюся нелепость выражения, данная схема работает.
		//0-ли в ней не формируются, если развернуть, то получится несколько другое,
		//получиться, классическое "отбеливание" данных: n^=E(n-1).

		n[3]^=n[2]^=n[1]^=n[0]^=MB_R0(n, 4);	//сдвиг на 23
		n[2]^=n[1]^=n[0]^=n[3]^=MB_R1(n, 4);	//сдвиг на 17
		n[1]^=n[0]^=n[3]^=n[2]^=MB_R0(n, 4);
		n[0]^=n[3]^=n[2]^=n[1]^=MB_R1(n, 4);
	}
	//окончательно отбеливание:  x^=E(n)
	x[0] ^= n[0], x[1] ^= n[1], x[2] ^= n[2], x[3] ^= n[3];
}

*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

//общее число ключей
#ifndef MB_X_MAX
	#define MB_X_MAX	4
#endif

//log2(64)=6 - минимальное кол-во циклов, для смены половины бит в слове.
#define MB_RX			6

typedef uint64_t 		uintxx_t;

//мин кол-во раундов, можно *4
#define MB_R_MAX 		(2*MB_RX)
//#define MB_R_MAX 		(MB_X_MAX*MB_RX)	//если не хватит 12 циклов

//константы для генератора
//0x9E3779B97F4A7C15,0xB7E151628AED2A6B,
//0x2E8C480C6E2A0001,0x904BF6BB31D1549C,
//0x9E3779B97F4A7C15,0x48080B0400FF370A,
//0xA836086390A04EA0,0xB7E151628AED2A6B,
//0xD52F0DA63A00A429
#define MB_C0			0x2E8C480C6E2A0001
#define MB_C1			0x904BF6BB31D1549C

//длина ключевой информации в байта и битах
#define MB_X_BYTE_MAX		(sizeof(uintxx_t)*MB_X_MAX)	//в байтах
#define MB_X_BITS_MAX		(MB_X_BYTE_MAX*8)		//в битах

//генератор последовательности
#define MB_NEXT_GEN()		n[0]+=MB_C0,n[1]+=MB_C1; if (n[1]<MB_C1) n[1]++;

//сдвиги
#define MB_R0(xx,nn)		__mb_rot23(__mb_sum(xx,nn))
#define MB_R1(xx,nn)		__mb_rot17(__mb_sum(xx,nn))

//здесь можно aes или "кузнечик", пока не используется
static const uint8_t __MB_TAB_SBOX[] = 
{
	0x63,0x7C,0x77,0x7B,0xF2,0x6B,0x6F,0xC5,0x30,0x01,0x67,0x2B,0xFE,0xD7,0xAB,0x76,
	0xCA,0x82,0xC9,0x7D,0xFA,0x59,0x47,0xF0,0xAD,0xD4,0xA2,0xAF,0x9C,0xA4,0x72,0xC0,
	0xB7,0xFD,0x93,0x26,0x36,0x3F,0xF7,0xCC,0x34,0xA5,0xE5,0xF1,0x71,0xD8,0x31,0x15,
	0x04,0xC7,0x23,0xC3,0x18,0x96,0x05,0x9A,0x07,0x12,0x80,0xE2,0xEB,0x27,0xB2,0x75,
	0x09,0x83,0x2C,0x1A,0x1B,0x6E,0x5A,0xA0,0x52,0x3B,0xD6,0xB3,0x29,0xE3,0x2F,0x84,
	0x53,0xD1,0x00,0xED,0x20,0xFC,0xB1,0x5B,0x6A,0xCB,0xBE,0x39,0x4A,0x4C,0x58,0xCF,
	0xD0,0xEF,0xAA,0xFB,0x43,0x4D,0x33,0x85,0x45,0xF9,0x02,0x7F,0x50,0x3C,0x9F,0xA8,
	0x51,0xA3,0x40,0x8F,0x92,0x9D,0x38,0xF5,0xBC,0xB6,0xDA,0x21,0x10,0xFF,0xF3,0xD2,
	0xCD,0x0C,0x13,0xEC,0x5F,0x97,0x44,0x17,0xC4,0xA7,0x7E,0x3D,0x64,0x5D,0x19,0x73,
	0x60,0x81,0x4F,0xDC,0x22,0x2A,0x90,0x88,0x46,0xEE,0xB8,0x14,0xDE,0x5E,0x0B,0xDB,
	0xE0,0x32,0x3A,0x0A,0x49,0x06,0x24,0x5C,0xC2,0xD3,0xAC,0x62,0x91,0x95,0xE4,0x79,
	0xE7,0xC8,0x37,0x6D,0x8D,0xD5,0x4E,0xA9,0x6C,0x56,0xF4,0xEA,0x65,0x7A,0xAE,0x08,
	0xBA,0x78,0x25,0x2E,0x1C,0xA6,0xB4,0xC6,0xE8,0xDD,0x74,0x1F,0x4B,0xBD,0x8B,0x8A,
	0x70,0x3E,0xB5,0x66,0x48,0x03,0xF6,0x0E,0x61,0x35,0x57,0xB9,0x86,0xC1,0x1D,0x9E,
	0xE1,0xF8,0x98,0x11,0x69,0xD9,0x8E,0x94,0x9B,0x1E,0x87,0xE9,0xCE,0x55,0x28,0xDF,
	0x8C,0xA1,0x89,0x0D,0xBF,0xE6,0x42,0x68,0x41,0x99,0x2D,0x0F,0xB0,0x54,0xBB,0x16
};

//преобразование по таблице
static inline void __mb_tab_sbox(uint8_t *x, int x_len)
{
	while(x_len--)
		x[x_len] = __MB_TAB_SBOX[x[x_len]];
}


//сдвиг, 23,17
static inline uintxx_t __mb_rot23(uintxx_t x) { return (x << 23) |(x >> 41); }
static inline uintxx_t __mb_rot17(uintxx_t x) { return (x << 17) |(x >> 47); }

/*
static inline uintxx_t __mb_rotN(uintxx_t x, int n)
{
	return (x << n) |(x >> (64 - n));
}
*/

//сумма всех элементов
static inline uintxx_t __mb_sum(uintxx_t *x, int len)
{
	uintxx_t s =0; while(len--) s +=x[len]; return s;
}


//просто распечатеть блок в формате hex-чисел
static inline void __mb_print(uintxx_t *x, int len)
{
	printf("n[]=");
	for (int i =len; i; i--) {
		printf("%016llX ", x[i-1]);
	}
}

//печатаем блок и сумму
static inline void __mb_print_sum(uintxx_t *x, int len)
{
	__mb_print(x, len);
	printf("sum(n[])=%016llx\n", __mb_sum(x, len));
}


//базовая реализация: 128бит
static inline void __mb_next_2(uintxx_t *x)
{
	int i;
	//делаем локальную копию
	uintxx_t n[2];
	n[0] =x[0], n[1] =x[1];

	//если надо конвертануть по таблице
	//__mb_tab_sbox((uint8_t *)&n, sizeof(n));

	//генератор
	MB_NEXT_GEN();
	//микс отбеливания
	for(i =0; i <MB_R_MAX; i++) {
		n[1] ^=n[0] ^=MB_R0(n, 2);
		n[0] ^=n[1] ^=MB_R1(n, 2);
	}
	//окончательно отбеливание
	x[0] ^= n[0], x[1] ^= n[1];
}

//базовая реализация: 256 бит
static inline void __mb_next_4(uintxx_t *x)
{
	int i;
	uintxx_t n[4];
	n[0] =x[0], n[1] =x[1], n[2] =x[2], n[3] =x[3];

	//если надо конвертануть по таблице
	//__mb_tab_sbox((uint8_t *)&n, sizeof(n));

	MB_NEXT_GEN();
	for(i =0; i <MB_R_MAX; i++) {
		n[3]^=n[2]^=n[1]^=n[0]^=MB_R0(n, 4);
		n[2]^=n[1]^=n[0]^=n[3]^=MB_R1(n, 4);
		n[1]^=n[0]^=n[3]^=n[2]^=MB_R0(n, 4);
		n[0]^=n[3]^=n[2]^=n[1]^=MB_R1(n, 4);
	}
	x[0] ^= n[0], x[1] ^= n[1], x[2] ^= n[2], x[3] ^= n[3];
}

//384бит
static inline void __mb_next_6(uintxx_t *x)
{
	int i;
	uintxx_t n[6];
	n[0] =x[0], n[1] =x[1], n[2] =x[2];
	n[3] =x[3], n[4] =x[4], n[5] =x[5];

	//если надо конвертануть по таблице
	//__mb_tab_sbox((uint8_t *)&n, sizeof(n));

	MB_NEXT_GEN();
	for(i =0; i <MB_R_MAX; i++) {
		n[5]^=n[4]^=n[3]^=n[2]^=n[1]^=n[0]^=MB_R0(n, 6);
		n[4]^=n[3]^=n[2]^=n[1]^=n[0]^=n[5]^=MB_R1(n, 6);
		n[3]^=n[2]^=n[1]^=n[0]^=n[5]^=n[4]^=MB_R0(n, 6);
		n[2]^=n[1]^=n[0]^=n[5]^=n[4]^=n[3]^=MB_R1(n, 6);
		n[1]^=n[0]^=n[5]^=n[4]^=n[3]^=n[2]^=MB_R0(n, 6);
		n[0]^=n[5]^=n[4]^=n[3]^=n[2]^=n[1]^=MB_R1(n, 6);
	}
	x[0] ^= n[0], x[1] ^= n[1], x[2] ^= n[2];
	x[3] ^= n[3], x[4] ^= n[4], x[5] ^= n[5];
}


//базовая реализация: 512 бит
static inline void __mb_next_8(uintxx_t *x)
{
	int i;
	uintxx_t n[8];
	n[0] =x[0], n[1] =x[1], n[2] =x[2], n[3] =x[3];
	n[4] =x[4], n[5] =x[5], n[6] =x[6], n[7] =x[7];

	//если надо конвертануть по таблице
	//__mb_tab_sbox((uint8_t *)&n, sizeof(n));

	MB_NEXT_GEN();
	for(i =0; i <MB_R_MAX; i++) {
		n[7]^=n[6]^=n[5]^=n[4]^=n[3]^=n[2]^=n[1]^=n[0]^=MB_R0(n, 8);
		n[6]^=n[5]^=n[4]^=n[3]^=n[2]^=n[1]^=n[0]^=n[7]^=MB_R1(n, 8);
		n[5]^=n[4]^=n[3]^=n[2]^=n[1]^=n[0]^=n[7]^=n[6]^=MB_R0(n, 8);
		n[4]^=n[3]^=n[2]^=n[1]^=n[0]^=n[7]^=n[6]^=n[5]^=MB_R1(n, 8);
		n[3]^=n[2]^=n[1]^=n[0]^=n[7]^=n[6]^=n[5]^=n[4]^=MB_R0(n, 8);
		n[2]^=n[1]^=n[0]^=n[7]^=n[6]^=n[5]^=n[4]^=n[3]^=MB_R1(n, 8);
		n[1]^=n[0]^=n[7]^=n[6]^=n[5]^=n[4]^=n[3]^=n[2]^=MB_R0(n, 8);
		n[0]^=n[7]^=n[6]^=n[5]^=n[4]^=n[3]^=n[2]^=n[1]^=MB_R1(n, 8);
	}
	x[0] ^= n[0], x[1] ^= n[1], x[2] ^= n[2], x[3] ^= n[3];
	x[4] ^= n[4], x[5] ^= n[5], x[6] ^= n[6], x[7] ^= n[7];
}



//с разбором макроопределений
static inline void __mb_next(uintxx_t *x)
{
	//быстрая реализация
	#if (MB_X_MAX==2)
		__mb_next_2(x);
	#elif (MB_X_MAX==4)
		__mb_next_4(x);
	#elif (MB_X_MAX==6)
		__mb_next_6(x);
	#elif (MB_X_MAX==8)
		__mb_next_8(x);
	#else
		#error "key size: 2,4,6,8"
	#endif
}



//добавить в поток строку
static inline void __mb_add_next(uintxx_t *x, uint8_t *s, uintxx_t s_len)
{
	int i;
	uintxx_t len;
	uint8_t *bx =(uint8_t *)x;

	//миксуем длину строки
	x[0] +=s_len;
	__mb_next(x);

	if (0 <s_len) {
		do {
			//миксуем кусочками по размеру X
			len = (s_len <MB_X_BYTE_MAX)? s_len: MB_X_BYTE_MAX;
			//addимся по байтам
			for(i =0; i <len; i++) bx[i] +=s[i];

			__mb_next(x);

			s_len -=len; s +=len;
		} while (s_len);
	}
}


/*
 * РАСШИРЕНИЯ ДЛЯ УДОБСТВА, API
 */


//структура для работы
typedef struct {
	uintxx_t X[MB_X_MAX];	//список ключей
	uint64_t L;		//использованная длина
} __mb_ctx_t;


//init для структуры
static inline void __mb_init(__mb_ctx_t *e)
{
	memset(e, 0, sizeof(__mb_ctx_t));

	//или например так, для бадабума
	//memcpy(e, __MB_TAB_SBOX, sizeof(__mb_ctx_t));
}

//обновить данные строкой, без учета длины.
static inline void __mb_update(__mb_ctx_t *e, uint8_t *s, uint64_t s_len)
{
	int i;
	uint64_t len;
	uint8_t *bx =(uint8_t *)e->X;

	//фиксируем длину
	e->L +=s_len;

	if (0 <s_len) {
		do {	//миксуем кусочками по размеру X
			len = (s_len <MB_X_BYTE_MAX)? s_len: MB_X_BYTE_MAX;
			//addимся по байтам
			for(i =0; i <len; i++) bx[i] +=s[i];

			__mb_next(e->X);

			s_len -=len; s +=len;
		} while (s_len);
	}
}

//финал для хеша, финал длиной строки + вывод
static inline void __mb_final(__mb_ctx_t *e, uintxx_t *out, int out_len)
{
	int i;
	uint64_t len =e->L;
	//добавим последний штрих, длина сообщения
	__mb_update(e, (uint8_t *) &len, sizeof(len));
	//проверим выходную длину
	out_len =(out_len <MB_X_MAX) ?out_len :MB_X_MAX;
	//отдаем
	if (out)
		for (i=0; i<out_len; i++) out[i] = e->X[i];	
}


//де/кодировать строку
static inline void __mb_code(__mb_ctx_t *e, uint8_t *s_inp, uint64_t s_len, uint8_t *s_out)
{
	int i;
	uint64_t len;
	uint8_t *bx =(uint8_t *)e->X;

	if (0 <s_len) {
		do {
			//получаем ключ
			__mb_next(e->X);
			//вычисляем остаток строки
			len = (s_len <MB_X_BYTE_MAX)? s_len: MB_X_BYTE_MAX;
			//xorимся по байтам
			for(i =0; i <len; i++) s_out[i] =bx[i] ^ s_inp[i];
			//корректируеме остатки
			s_len -=len; s_inp +=len; s_out +=len;
		} while (s_len);
	}
}



//мак-хеш для 1й-строки
static inline int __mb_hash_str(uint8_t *str, uint64_t len_str, uintxx_t *out, int out_len)
{
	__mb_ctx_t e;

	__mb_init(&e);
	__mb_update(&e, str, len_str);
	__mb_final(&e, out, out_len);

	return 0;
}



/*
** ПРИМЕР MAC/HASH ДЛЯ ФАЙЛА
*/
static inline int __mb_hash_file(char *name, uint8_t *pwd, int len_pwd, uintxx_t *out, int out_len)
{
	uint8_t tmp[MB_X_BYTE_MAX*1024];
	__mb_ctx_t e;

	FILE *fp;
	uint64_t r;

	__mb_init(&e);
	__mb_update(&e, pwd, len_pwd);

	fp = fopen(name,"rb");
	if (!fp)
		return -1; 

	while(1) {
		//read block
		r =(uint64_t)fread(&tmp, 1, sizeof(tmp), fp);
		if (r ==0) break;
		// добавляем блоками
		__mb_update(&e, tmp, r);
	}	
	fclose(fp);

	//отдаем mac/hash
	__mb_final(&e, out, out_len);

	return 0;

}


//де/кодировать файл
static inline int __mb_code_file(char *name, uint8_t *pwd, int len_pwd)
{
	//буфер для данных
	uint8_t tmp[MB_X_BYTE_MAX*1024];
	__mb_ctx_t e;
	int rc =0;
	FILE *fi =NULL,*fo =NULL;
	uint64_t r, len =0;
	char name_out[1024];

	__mb_init(&e);
	__mb_update(&e, pwd, len_pwd);
	__mb_final(&e, NULL, 0);

	do {
		//входящий файл
		fi = fopen(name,"rb");
		if (!fi) { rc =-1; break; }
		//исходящий
		snprintf(name_out,sizeof(name_out),"%s.mb",name);
		fo = fopen(name_out,"wb");
		if (!fi) { rc =-2; break; }

		//обработка 
		while(1) {
			//read block
			r =(uint64_t)fread(&tmp, 1, sizeof(tmp), fi);
			//если закончили
			if (r ==0) break;
			//кодируем
			__mb_code(&e, (uint8_t *)&tmp, r, (uint8_t *)&tmp);
			//записали?
			len =(uint64_t)fwrite(&tmp, 1, r, fo);
			if (len != r) { rc =-3; break; }
		}
		//ok?		
	} while(0);

	//закрываем потоки
	if (fi)
		fclose(fi);
	if (fo)
		fclose(fo);

	return rc;
}


//проверяем пароль на валидность, ret: <0 -err, =0 ok
static inline int __mb_pwd_valid(uint8_t *pwd, int len)
{
	int i, rc=0;
	char *l ="qwertyuiopasdfghjklzxcvbnm";
	char *u ="QWERTYUIOPASDFGHJKLZXCVBNM";
	char *n ="1234567890";
	char *p ="~!@#$%^&*()_+-<>;\"'[]{}/\\";

	int fl =0,fu =0,fn =0,fp =0;

	if (len<6)
		return -6;

	//соседние знаки должны различатся больше чем на 1
	for (i =0; i <(len -1); i++) {
		if ((pwd[i] +1) == pwd[i +1]) 
			return -1;
	}

	//должно быть 4 группы символов
	for (i =0; i <len; i++) {
		if ((fl ==0) && strchr(l, pwd[i])) { fl =1; } 
		if ((fu ==0) && strchr(u, pwd[i])) { fu =1; } 
		if ((fn ==0) && strchr(l, pwd[i])) { fn =1; } 
		if ((fp ==0) && strchr(l, pwd[i])) { fp =1; }
	}

	//чего то нехватает
	if (!fl || !fu || !fn || !fp)
		return -4;

	//нормалбно все элементы есть
	return 0;
}


#endif

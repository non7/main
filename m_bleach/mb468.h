#ifndef __mb64_4_h
#define __mb64_4_h 1

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#define MB_C0 		0x0101010101010101
#define MB_C1		0x0101010101010104

#ifndef MB_MAX
	#define MB_MAX	4
#endif

#define MB_BYTE_MAX	(sizeof(uint64_t)*MB_MAX)
#define MB_ROUND_MAX	12

typedef struct {
	uint64_t G[2],S[MB_MAX];	//generate_128_bits + state_256_bits
	uint8_t  N;			//счетчик выданных байт, 0..31
} mb64_ctx_t;


static inline uint_fast64_t __R23(uint_fast64_t x) { return (x << 23) | (x >> 41); }
static inline uint_fast64_t __R17(uint_fast64_t x) { return (x << 17) | (x >> 47); }

#define __PP4(x)								\
	printf("%02d:%016llx %016llx - %016llx %016llx %016llx %016llx\n",	\
	x, g0,g1,s0,s1,s2,s3)


//основное обновление
static inline void __mb4(mb64_ctx_t *e)
{
	int i;
	uint_fast64_t g0,g1, s0,s1,s2,s3;

	//гамма+состояние
	g0 = e->G[0], g1 = e->G[1];
	s0 = e->S[0], s1 = e->S[1], s2 = e->S[2], s3 = e->S[3];

	//генератор и блокировка 0-го потока
	g0 += MB_C0, g1 += MB_C1; if (g1 < MB_C1) g1++;

	for (i = 0; i < MB_ROUND_MAX; i++) {
		g0 ^= s3 ^= s2 ^= s1 ^= s0 ^= __R23(g1 + s0 + s1 + s2 + s3);
		g1 ^= s0 ^= s1 ^= s2 ^= s3 ^= __R17(g0 + s0 + s1 + s2 + s3);
	}

	e->S[0] ^= s0, e->S[1] ^= s1, e->S[2] ^= s2, e->S[3] ^= s3;
	e->G[0] ^= g0, e->G[1] ^= g1;
}

#define __PP6(x)										\
	printf("%02d:%016llx %016llx - %016llx %016llx %016llx %016llx %016llx %016llx\n",	\
	x, g0,g1,s0,s1,s2,s3,s4,s5)

static inline void __mb6(mb64_ctx_t *e)
{
	int i;
	uint_fast64_t g0,g1, s0,s1,s2,s3,s4,s5;

	//гамма+состояние
	g0 = e->G[0], g1 = e->G[1];
	s0 = e->S[0], s1 = e->S[1], s2 = e->S[2],
	s3 = e->S[3], s4 = e->S[4], s5 = e->S[5];

	//генератор и блокировка 0-го потока
	g0 += MB_C0, g1 += MB_C1; if (g1 < MB_C1) g1++;

	for (i = 0; i < MB_ROUND_MAX; i++) {
		g0 ^= s5 ^= s4 ^= s3 ^= s2 ^= s1 ^= s0 ^=
			__R23(g1 + s0 + s1 + s2 + s3 + s4 + s5);
		g1 ^= s0 ^= s1 ^= s2 ^= s3 ^= s4 ^= s5 ^=
			__R17(g0 + s0 + s1 + s2 + s3 + s4 + s5);
	}

	e->S[0] ^= s0, e->S[1] ^= s1, e->S[2] ^= s2,
	e->S[3] ^= s3, e->S[4] ^= s4, e->S[5] ^= s5;
	e->G[0] ^= g0, e->G[1] ^= g1;
}


#define __PP8(x)												\
	printf("%02d:%016llx %016llx - %016llx %016llx %016llx %016llx %016llx %016llx %016llx %016llx\n",	\
	x, g0,g1,s0,s1,s2,s3,s4,s5,s6,s7)

static inline void __mb8(mb64_ctx_t *e)
{
	int i;
	uint_fast64_t g0,g1, s0,s1,s2,s3,s4,s5,s6,s7;

	//гамма+состояние
	g0 = e->G[0], g1 = e->G[1];
	s0 = e->S[0], s1 = e->S[1], s2 = e->S[2], s3 = e->S[3],
	s4 = e->S[4], s5 = e->S[5], s6 = e->S[6], s7 = e->S[7];

//__PP8(-2);

	//генератор и блокировка 0-го потока
	g0 += MB_C0, g1 += MB_C1; if (g1 < MB_C1) g1++;
//__PP8(-1);

	for (i = 0; i < MB_ROUND_MAX; i++) {
		g0 ^= s7 ^= s6 ^= s5 ^= s4 ^= s3 ^= s2 ^= s1 ^= s0 ^=
			__R23(g1 + s0 + s1 + s2 + s3 + s4 + s5 + s6 + s7);
//__PP8(i);

		g1 ^= s0 ^= s1 ^= s2 ^= s3 ^= s4 ^= s5 ^= s6 ^= s7 ^=
			__R17(g0 + s0 + s1 + s2 + s3 + s4 + s5 + s6 + s7);
//__PP8(i);
	}

	e->S[0] ^= s0, e->S[1] ^= s1, e->S[2] ^= s2, e->S[3] ^= s3,
	e->S[4] ^= s4, e->S[5] ^= s5, e->S[6] ^= s6, e->S[7] ^= s7;
	e->G[0] ^= g0, e->G[1] ^= g1;
}


#if   (MB_MAX==4)
	#define mb __mb4
#elif (MB_MAX==6)
	#define mb __mb6
#elif (MB_MAX==8)
	#define mb __mb8
#else
	#error "MB_MAX set: 4,6,8!"
#endif

//пропишем константы, что бы с пустого не стартовать
//хотя алгоритму всё равно, если есть '1', то развернет в любом случае
//с ними просто не тратим лишнее
static void mb_init(mb64_ctx_t *e)
{
	int i;

	static const uint64_t __G[2] = {
		0xCBBB9D5DC1059ED8,0x629A292A367CD507
	};
	static const uint64_t __S[8] = {
		0x6A09E667F3BCC908,0xBB67AE8584CAA73B,
		0x3C6EF372FE94F82B,0xA54FF53A5F1D36F1,
		0x510E527FADE682D1,0x9B05688C2B3E6C1F,
		0x1F83D9ABFB41BD6B,0x5BE0CD19137E2179
	};

//	memset(e,0, sizeof(mb64_ctx_t));
//	return;

	//счетчик выданных байт
	e->N = 0;
	//гамма
	e->G[0] = __G[0], e->G[1] = __G[1];

	//состояние
	for (i = 0; i < MB_MAX; i++)
		e->S[i] = __S[i];
}

//подмешать строку или для хеш
static void mb_update(mb64_ctx_t *e, char *str, int str_len)
{
	int i, len;
	uint8_t *bs = (uint8_t *)&e->S;

	if (str_len > 0) {
		//миксуем длину строки
		e->G[0] += (uint64_t)str_len;

		do {
			//миксуем кусочками по размеру N[4]
			len = (str_len < MB_BYTE_MAX)? str_len: MB_BYTE_MAX;
			//addимся по байтам
			for (i = 0; i < len; i++) {
				bs[i] += str[i];
			}

			mb(e);

			str_len -= len; str += len;
		} while (str_len);
	}
}

//вернет 1 байт или сделает шаг в генерации
static uint8_t mb_get_byte(mb64_ctx_t *e)
{
	uint_fast8_t *bs = (uint_fast8_t *) &e->S;

	if (e->N >= MB_BYTE_MAX) {
		//обновляем состояние
		mb(e);
		e->N = 0;
	}

	return bs[e->N++];
}

#endif

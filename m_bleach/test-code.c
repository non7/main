#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

#include "mb.h"

int main(int argc, char **argv)
{
	int i,rc;
	if (argc<2) {
		printf("use: test-code pwd file1... fileN\n");
		return -1;
	}

	for(i = 2; i < argc; i++) {
		printf("code: %s -> %s.mb -> ",argv[i], argv[i]);
		rc = __mb_code_file(argv[i], argv[1], strlen(argv[1]));
		printf("%s\n", ((rc==0)?"ok":"err"));
	}

	return 0;
}

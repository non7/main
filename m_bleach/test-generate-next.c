#include "mb.h"

#define SZ_1M ((1024*1024)/sizeof(uintxx_t))

static uintxx_t buf[SZ_1M];
static uintxx_t key[MB_X_MAX];

int
main(int argc, char **argv)
{
	FILE *fp;
	long i,j,k,sz=0;

	if (argc<3) {
     		printf("use: generate pwd sizeM fileout.bin\n");
     		return -1;
  	}

	printf("mode_x64=%d,key_size=%zd\n", (sizeof(uintxx_t)==8), sizeof(key)*8);

	__mb_add_next(key, argv[1], strlen(argv[1]));

	sz =atol(argv[2]);
	fp =fopen(argv[3],"wb"); if (!fp) { printf("err\n"); return -2; }

	for(i=0; i<sz; i++) {
		for (j=0; j<SZ_1M; j+=MB_X_MAX) {
			__mb_next(key);
     			for(k=0; k<MB_X_MAX; k++) buf[j+k] =key[k];
		}

		fwrite(&buf,1,sizeof(buf),fp);
	}	

	fclose(fp);

	return 0;
}



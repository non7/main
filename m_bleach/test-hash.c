#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

#include "mb.h"

int main(int argc, char **argv)
{
	uintxx_t out[MB_X_MAX];
	int i;
	if (argc<2) {
		printf("use: test-hash pwd file1... fileN\n");
		return -1;
	}

	for(i = 2; i < argc; i++) {

		__mb_hash_file(argv[i], argv[1], strlen(argv[1]), out, MB_X_MAX);
		__mb_print(out, MB_X_MAX); printf("*%s\n", argv[i]);
	}

	return 0;
}

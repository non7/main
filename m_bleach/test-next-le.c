#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

#include "mb.h"
#include "C:\crypto\my_lib\my.crylib.c"

int main(int argc, char **argv)
{
	uintxx_t tt=0;
	uintxx_t k[MB_X_MAX],k0[MB_X_MAX];
	int i,n,j;

	printf("mode_x64=%d,key_size=%zd\n", (sizeof(uintxx_t)==8), sizeof(k)*8);

	memset(k0, 0, sizeof(k0));

	k0[0] =1;
	for (i =0; i <MB_X_BYTE_MAX*8; i++) {
		__mb_print(k0, MB_X_MAX);printf(" -> ");
		memcpy(k, k0, sizeof(k0));
		__mb_next(k);
		__mb_print(k, MB_X_MAX); printf("\n");
		LNW64(k0, MB_X_MAX, 1);
	}
	printf("\n\n");

}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

#include "mb.h"



void main(int argc, char **argv)
{
	int i, j;
	uintxx_t x[256];


	for (j=2; j<=256; j++) {
		memset(x,0,sizeof(x));

		printf("%4d: -------------------------------------------->\n",j);
		for (i=0; i<16; i++) {
			__mb_next_nn(x,j);

			printf("%4d: ",i);
			__mb_print(x, j); printf("\n");
		}
		printf("\n");
	}
}

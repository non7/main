#include "mb.h"

int main(int argc, char **argv)
{
	uint64_t  tim;
	uint64_t i, j, n;
	uint64_t SS,NN;
	uintxx_t key[MB_X_MAX];

	printf("mode_x64=%d,key_size=%zd\n", (sizeof(uintxx_t)==8), sizeof(key)*8);

//CLOCKS_PER_SEC=1000
	for (n =0x10000, tim =0; tim <CLOCKS_PER_SEC; n <<=1) {
   		tim = clock();
   		for (j =0; j <n; j++) {
			__mb_next(key);
		}
   		tim =clock() -tim;

   		NN =n *(sizeof(key));
   		SS =CLOCKS_PER_SEC *NN/tim;

   		if (tim >0) {
     			printf("%lld B/s msec=%lld Bytes=%lld\n",SS, tim, NN);
     			fflush(stdout);
   		}
 	}
	printf("\n");
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

#include "mb.h"

int main(int argc, char **argv)
{
	int i,rc;
	if (argc<2) {
		printf("use: test-validpwd pwd1 .. pwdN\n");
		return -1;
	}

	for(i = 1; i < argc; i++) {
		printf("valid: %s -> ",argv[i]);
		rc = __mb_pwd_valid(argv[i], strlen(argv[i]));
		printf("%s\n", ((rc==0)?"ok":"err"));
	}

	return 0;
}

#include "rc43.h"


//следующий байт
uint16_t rc43(rc43_ctx_t *state, uint16_t b)
{
	uint16_t i, j, k, t;

	//get index
	i =state->I;
	j =state->J;
	k =state->K;

	//next index
	i++,                i &=RC43_S_MASK;	//для полного цикла, дает из 1024 -> 1022 изменений
//	i +=state->S[k] +1, i &=RC43_S_MASK;	//для полного цикла, дает из 1024 ->  970 изменений
	j +=state->S[i] +b, j &=RC43_S_MASK;
	k +=state->S[j],    k &=RC43_S_MASK;

	//swap
		  t =state->S[i];
	state->S[i] =state->S[j];
	state->S[j] =state->S[k];
	state->S[k] =t;

	//save index
	state->I =i;
	state->J =j;
	state->K =k;

	//next byte
	t =(state->S[i] +state->S[j] +state->S[k]) &RC43_S_MASK;

	return state->S[t];
}


//установить начальные значения
void rc43_set (rc43_ctx_t *state, uint8_t *pwd, int pwd_len)
{
	int i;

	memset(state, 0, sizeof(rc43_ctx_t));

	state->I =1;
	state->J =11;
	state->K =111;
	state->T =127;

	for (i=0; i<RC43_S_MAX; i++)
		state->S[i] =i;

	rc43_add (state,pwd,pwd_len);	//добавим пароль
	rc43_hash(state);		//сформируем начальный  hash
}


//добавить строку в состояние
void rc43_add (rc43_ctx_t *state, uint8_t *str, int str_len)
{
//	int i;
	uint16_t t =state->T;

//	for (i =0; i <str_len; i++) {
//		t +=rc43(state, t +str[i]);
//	}

	//эта версия побыстрей

	int n = (str_len + 7) / 8;

	//он сначала отработает остаток от деления, а потом всё остальное по 8 
	switch (str_len % 8) {
	case 0: do { 	t +=rc43(state, t + *str++);
	case 7:		t +=rc43(state, t + *str++);
	case 6: 	t +=rc43(state, t + *str++);
	case 5:		t +=rc43(state, t + *str++);
	case 4: 	t +=rc43(state, t + *str++);
	case 3:		t +=rc43(state, t + *str++);
	case 2: 	t +=rc43(state, t + *str++);
	case 1:		t +=rc43(state, t + *str++);
		} while (--n > 0);
	}

	state->T +=t;
}


//по состоянию формируем хеш
void rc43_hash(rc43_ctx_t *state)
{
	int i, j;
	uint16_t t =state->T;

	for (i=0; i<RC43_S_MAX; i+=RC43_H_MAX) {

		for (j=0; j<RC43_H_MAX; j+=8) {

			t +=state->H[j+0] ^=rc43(state, state->H[j+0] +t);
			t +=state->H[j+1] ^=rc43(state, state->H[j+1] +t);
			t +=state->H[j+2] ^=rc43(state, state->H[j+2] +t);
			t +=state->H[j+3] ^=rc43(state, state->H[j+3] +t);
			t +=state->H[j+4] ^=rc43(state, state->H[j+4] +t);
			t +=state->H[j+5] ^=rc43(state, state->H[j+5] +t);
			t +=state->H[j+6] ^=rc43(state, state->H[j+6] +t);
			t +=state->H[j+7] ^=rc43(state, state->H[j+7] +t);
		}
	}

	state->T +=t;
}


//распечатать состояние 
void rc43_print_state(rc43_ctx_t *state)
{
	int i;

	printf("\nI=%d,J=%d,K=%4d,T=%d,S[]=",state->I,state->J,state->K,state->T);

	for (i=0; i<RC43_S_MAX; i++) {
		if (i%16 ==0)
			printf("\n");
		printf("%04X ",state->S[i]);
	}

	printf("\nH[]=");
	for (i=0; i<RC43_H_MAX; i++) {
		if (i%16 ==0)
			printf("\n");
		printf("%04X ",state->H[i]);
	}

	printf("\n");

}





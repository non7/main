/*
	RC43, генератор с 3-мя индексами (почти rc4)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

//все размеры кратны степени 2: 256,512,1024,2048, ... 65536

//размер S-box в байтах
#define RC43_S_MAX 	1024
#define RC43_S_MASK 	(RC43_S_MAX-1)

//размер hash в байтах, не менее 8
#define RC43_H_MAX  	16 //(RC43_S_MAX/16)
#define RC43_H_MASK 	(RC43_H_MAX-1)


//структура rc43, с 3-мя индексами
typedef struct {
	uint16_t	I, J, K, T;	//I,J,K -index, T -временный аккумулятор
	uint16_t	S[RC43_S_MAX];	//S-box
	uint16_t	H[RC43_H_MAX];	//H-hash
} rc43_ctx_t;

//генерируем сл.байт
uint16_t rc43(rc43_ctx_t *state, uint16_t b);
void rc43_set (rc43_ctx_t *state, uint8_t *pwd, int pwd_len);
void rc43_add (rc43_ctx_t *state, uint8_t *str, int str_len);
void rc43_hash(rc43_ctx_t *state);
void rc43_print_state(rc43_ctx_t *state);


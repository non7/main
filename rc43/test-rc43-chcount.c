/*
	RC43 с rc4 3-мя индексами
*/

#include "rc43.h"

void rc43_print_hex(rc43_ctx_t *state)
{
	rc43_print_state(state);
}


//
//посчитать сколько байт состояния изменилось за 256 циклов вызовов
//в среднем должно  изменятся 242 байта S[] из 256
//минимум  =229, максимум =255
//
int rc43_chcount(rc43_ctx_t *a, rc43_ctx_t *b)
{
	int i,c =0;
	for (i=0; i<RC43_S_MAX; i++) {
		if (a->S[i] !=b->S[i])
			c++;
	}

	return c;
}

int main(int argc, char **argv)
{
	if (argc<2) {
		printf("use: test-rc43-chcount pwd\n");
		return 0;
	}

	rc43_ctx_t state, state_prev;
	int i, j, rc;
	uint8_t x =0;

	rc43_set(&state, argv[1], strlen(argv[1]));

	printf("pwd=%15s ",argv[1]);
	rc43_print_hex(&state);

	for (i=0; i<1024; i++) {
		memcpy(&state_prev, &state, sizeof(rc43_ctx_t));
		printf("%5d: ",i);
		for (j=0; j<RC43_S_MAX; j++) {
			x =rc43(&state, x);
			///printf ("%04x ",x);
		}

		rc =rc43_chcount(&state_prev, &state);
		printf(" ch=%d\n",rc);
	}
	printf ("\n");

	return 0;
}

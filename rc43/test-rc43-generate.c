/*
	RC43 с rc4 3-мя индексами
*/

#include "rc43.h"


static char buffer[1024*1024];

int main(int argc, char **argv)
{
	if (argc<3) {
		printf("use: test-rc43 pwd sizeMb file\n");
		return 0;
	}

	rc43_ctx_t state;
	int sz,i;
	uint8_t x =0;
	FILE *fp =fopen(argv[3],"wb");

	if (!fp)
		return -1;

	rc43_set(&state, argv[1], strlen(argv[1]));


	for (sz=0; sz<atoi(argv[2]); sz++) {

		for (i=0; i<sizeof(buffer); i++)
			buffer[i] =x =rc43(&state, x);

		fwrite(buffer,1,sizeof(buffer),fp);
	}

	fclose(fp);	

	return 0;
}

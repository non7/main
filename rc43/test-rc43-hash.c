/*
	RC43 с rc4 3-мя индексами
*/

#include "rc43.h"


static char buffer[1024*1024];

int main(int argc, char **argv)
{
	if (argc<2) {
		printf("use: test-rc43-hash str1 .. strN\n");
		return 0;
	}

	rc43_ctx_t state;
	int i,j;

	for (i=1; i<argc; i++) {
		rc43_set (&state, "", 0);
		rc43_add (&state, argv[i], strlen(argv[i]));
		rc43_hash(&state);

		for (j=0; j<RC43_H_MAX; j++) printf("%02X ",state.H[j]&255);
		printf("* %s\n", argv[i]);
	}

	return 0;
}
